<?php

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Load Env
 */
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('\App\Core\Error::errorHandler');
set_exception_handler('\App\Core\Error::exceptionHandler');

/**
 * Routing
 */
$router = new \App\Core\Router();

// Add the routes
$router->add('/user', [
    'controller' => 'UserController',
    'action' => 'getAllUser'
]);

$router->add('/user/info', [
    'controller' => 'UserController',
    'action' => 'userInfor'
]);

$router->add('/login', [
    'controller' => 'AuthController',
    'action' => 'login'
]);

$router->add('/logout', [
    'controller' => 'AuthController',
    'action' => 'logout'
]);

$router->add('/loginUser', [
    'controller' => 'AuthController',
    'action' => 'loginUser'
]);

$router->add('/', [
    'controller' => 'HomeController',
    'action' => 'index'
]);

$router->add('/getAllUser', [
    'controller' => 'UserController',
    'action' => 'getAllUser'
]);

$router->add('/user/add', [
    'controller' => 'UserController',
    'action' => 'addUser'
]);

$router->add('/add', [
    'controller' => 'UserController',
    'action' => 'add'
]);

$router->add('/register', [
    'controller' => 'AuthController',
    'action' => 'register'
]);

$router->add('/registeruser', [
    'controller' => 'AuthController',
    'action' => 'registeruser'
]);

$router->add('/edit/{id}', [
    'controller' => 'UserController',
    'action' => 'edit'
]);

$router->add('/edit/user/{id}', [
    'controller' => 'UserController',
    'action' => 'edituser'
]);

$router->add('/user/{id}', [
    'controller' => 'UserController',
    'action' => 'getUser'
]);

$router->add('/delete/{id}', [
    'controller' => 'UserController',
    'action' => 'delete'
]);

$router->dispatch($_SERVER['REQUEST_URI']);