<?php

namespace App\Core\Database;

interface BuilderInterface
{
    /**
     * Execute the query as a "select" statement.
     *
     * @param array $columns
     */
    public function get(array $columns = ['*']);

    /**
     * Execute a query for a single record by ID.
     *
     * @param int|string $id
     * @param array $columns
     */
    public function find(int|string $id, array $columns = ['*']);

    /**
     * Set the columns to be selected.
     *
     * @param array $columns
     * @return $this
     */
    public function select(array $columns = ['*']);

    /**
     * Add a basic where clause to the query.
     *
     * @param string $column
     * @param mixed|null $operator
     * @param mixed|null $value
     * @param string $boolean
     * @return $this
     */
    public function where(string $column, mixed $operator = null, mixed $value = null, string $boolean = 'and');

    /**
     * Update records in the database.
     *
     * @param  array  $values
     * @return int
     */
    public function update(array $values);

    /**
     * Insert new records into the database.
     *
     * @param  array  $values
     * @return bool
     */
    public function insert(array $values);

    /**
     * Delete records from the database.
     *
     * @param mixed|null $id
     * @return int
     */
    public function delete(mixed $id = null);
}