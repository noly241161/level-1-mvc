<?php

namespace App\Models;

use App\Core\Model;

class User extends Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAllUser()
    {
        $db = static::getDB();
        $sql = "SELECT * FROM user";
        $result = $db->query($sql);
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function login($username, $password)
    {
        $db = static::getDB();
        $sql = "select * from user where username = '$username' and password = '$password'";
        $result = $db->query($sql);
        return $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function checkUser($username)
    {
        $db = static::getDB();
        $sql = "SELECT * FROM user where username = '$username'";
        $result = $db->query($sql);
        return $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function getUserById($id)
    {
        $db = static::getDB();
        $sql = "select * from user where id = '$id'";
        $result = $db->query($sql);
        return $result->fetch(\PDO::FETCH_ASSOC);
    }

    public function createUser($user)
    {
        $db = static::getDB();
        $username = $user['username'];
        $password = $user['password'];
        $name = $user['name'];
        $user_type  = $user['user_type'];
        $sql = "insert into user values (Null, '$username','$password','$name',$user_type)";
        $result = $db->query($sql);
        return $result;
    }

    public function editUser($user)
    {
        $db = static::getDB();
        $id = $user['id'];
        $password = $user['password'];
        $name = $user['name'];

        $sql = "update  user set password = '$password',name = '$name' where id = '$id'";
        $result = $db->query($sql);
        return $result;
    }

    public function deleteUser($id)
    {
        $db = static::getDB();
        $sql = "delete from user where id = '$id'";
        $result = $db->query($sql);
        return $result;
    }
}