<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table,
th,
td {
    border: 1px solid black;
}
</style>

<body>
    <a href="/user/add"><button>them</button></a>
    <table style="width:75%">
        <tr>
            <th>stt</th>
            <th>name</th>
            <th>username</th>
            <th>user type</th>
            <th>action</th>
        </tr>
        <?php
        $i = 1;
        foreach ($data as $value) {
        ?>
        <tr>
            <td><?php echo $i++ ?></td>
            <td><?php echo $value['name'] ?></td>
            <td><?php echo $value['username'] ?></td>
            <td><?php echo $value['user_type'] == 1 ? 'admin' : 'user' ?></td>
            <td><a href="/user/<?php echo $value['id'] ?>"><button>view</button></a> | <a
                    href="/edit/<?php echo $value['id'] ?>"><button>edit</button></a> | <a
                    href="/delete/<?php echo $value['id'] ?>"><button>delete</button></a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>

</body>

</html>