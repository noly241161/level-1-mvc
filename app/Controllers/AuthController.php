<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;
use App\Models\User;

class AuthController extends Controller
{

    public function login()
    {
        View::render('login.php');
    }

    public function loginUser()
    {
        if (isset($_POST['login'])) {
            $username = $_POST["username"];
            $password = $_POST["password"];
        }
        $user = new User();

        $data = $user->login($username, $password);

        if ($data) {
            setcookie('id', $data['id'], time() + 6000, "/");
            setcookie('username', $data['username'], time() + 6000, "/");
            setcookie('password', $data['password'], time() + 6000, "/");
            setcookie('name', $data['name'], time() + 6000, "/");

            setcookie('user_type', $data['user_type'], time() + 6000, "/");

            header('Location: /user/info');
        } else {
            header('Location: /login');
        }
    }

    public function register()
    {
        View::render('register.php');
    }

    public function registeruser()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $user = new User();
        $data = [
            'username' => $username,
            'password' => $password,
            'name' => '',
            'user_type' => 0
        ];
        $checkuser = $user->checkUser($username);

        if (!isset($checkuser)) {
            echo 'tai khoang da ton tai';
        } else {
            $check = $user->createUser($data);
            if ($check === false) {
                echo 'Error creating';
            } else {
                View::render('login.php');
            }
        }
    }

    public  function logout()
    {
        setcookie('id', '');
        setcookie('username', '');
        setcookie('password', '');
        setcookie('name', '');
        setcookie('user_type', '');

        header('Location: /login');
    }
}