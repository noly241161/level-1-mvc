<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;
use App\Models\User;

class UserController extends Controller
{
    public function userInfor()
    {
        View::render('userInfo.php');
    }

    public function getAllUser()
    {
        if ($_COOKIE['user_type']) {
            $user = new User();
            $data = $user->getAllUser();

            View::render('user.php', compact('data'));
        } else {
            echo 'ban khong co quyen';
        }
    }

    public function addUser()
    {
        if ($_COOKIE['user_type']) {
            View::render('addUser.php');
        }
    }

    public function add()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $name = $_POST['name'];
        $user_type = $_POST['user_type'];

        if ($_COOKIE['user_type']) {
            $user = new User();
            $data = [
                'username' => $username,
                'password' => $password,
                'name' => $name,
                'user_type' => $user_type
            ];
            $checkuser = $user->checkUser($username);

            if (isset($checkuser)) {
                echo 'tai khoang da ton tai';
            } else {
                $check = $user->createUser($data);
                header('Location: /user/info');
            }
        } else {
            echo 'ban khong co quyen';
        }
    }

    public function edit($id)
    {
        $user = new User();

        $data = $user->getUserById($id);
        View::render('edit.php', compact('data'));
    }

    public function edituser($id)
    {
        // echo $id;
        $data = [
            'id' => $id,
            'name' => $_POST['name'],
            'password' => $_POST['password'],
        ];
        if ($id == $_COOKIE['id']) {
            $user = new User();
            $check = $user->editUser($data);
            header('Location: /user/info');
        } else {
            if ($_COOKIE['user_type']) {
                $user = new User();
                $check = $user->editUser($data);
                header('Location: /user/info');
            } else {
                echo 'ban khong co quyen';
            }
        }
    }

    public function getUser($id)
    {
        if ($_COOKIE['user_type']) {
            $user = new User();
            $data = $user->getUserById($id);

            View::render('UserById.php', compact('data'));
        } else {
            echo 'ban khong co quyen';
        }
    }

    public function delete($id)
    {
        if ($_COOKIE['user_type']) {
            $user = new User();
            $user->deleteUser($id);
            // $data = $user->getAllUser();
            // View::render('user.php', compact('data'));
            header('Location: /user');
        } else {
            echo 'ban khong co quyen';
        }
    }
}