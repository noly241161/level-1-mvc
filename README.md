# MVC
## MVC là gì
- là 1 mẫu kiến trúc thiết kế phần mềm. Gồm 3 phần chính Model-View-Controller với 3 nhiệm vụ khác nhau
- **Model** là là phần chứa phương thức sử lý, truy xuất database, đối tượng mô tả class, hàm sử lý
- **View** là phần đảm nhiệm hiển thị thông tin, tương tác với người dùng.
- **Controller** là phần điều hướng các hành động từ client, từ đó đưa ra các sử lý với data base nếu có
## Ưu nhược điểm mô hình mvc
- thể hiện tính chuyên nghiệp trongg lập trình
- chia thành các phần độc lập và các phần khác nhau dễ dàng nâng cấp, sử lý, làm việc nhóm

- tốc độ sử lý chậm hơn
- thời gian xây dựng lâu hơn